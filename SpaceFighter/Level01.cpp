
#include <iostream>
#include <string>
#include "Level01.h"
#include "BioEnemyShip.h"
#include "BossBioEnemyShip.h"

using namespace std; 
	


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	cout << "Level1";

	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	// Mod: changed cout to 25 from 21
	const int COUNT = 25;

	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55,
		0.25, 0.2, 0.3
	};
	
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};

	float delay = 1.0; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip *pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	// Setup enemy ships
	Texture *pTextureq = pResourceManager->Load<Texture>("Textures\\BossBioEnemyShip.png");

	double xPositionsq = 0.5;
	double delaysq = 5.0;
	Vector2 positionq;

	delay += delaysq;
	positionq.Set(xPositionsq * Game::GetScreenWidth(), -pTextureq->GetCenter().Y);

	BossBioEnemyShip *pEnemyq = new BossBioEnemyShip();
	pEnemyq->SetTexture(pTextureq);
	pEnemyq->SetCurrentLevel(this);
	pEnemyq->Initialize(positionq, (float)delay);
	AddGameObject(pEnemyq);

	Level::LoadContent(pResourceManager);
}