
#pragma once

#include "EnemyShip.h"

class BossBioEnemyShip : public EnemyShip
{

public:

	BossBioEnemyShip();
	virtual ~BossBioEnemyShip() { }

	void SetTexture(Texture *pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);

	// mod: changed text in the return string
	virtual std::string ToString() const { return "Boss Bio Enemy Ship"; }


private:

	Texture * m_pTexture;

};