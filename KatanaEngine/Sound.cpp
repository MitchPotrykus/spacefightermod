#include "KatanaEngine.h"
#include "Sound.h"

namespace KatanaEngine
{

	bool Sound::s_alAddonInitialized = false;

	Sound::Sound()
	{
		if (!s_alAddonInitialized)
		{
			al_init_acodec_addon();
			s_alAddonInitialized = true;
		}

		m_pSample = nullptr;
	}


	Sound::~Sound()
	{
		al_destroy_sample(m_pSample);
		m_pSample = nullptr;
	}

	bool Sound::Load(const std::string &path, ResourceManager *pManager)
	{
		ALLEGRO_SAMPLE *pTemp = al_load_sample(path.c_str());

		SetSample(pTemp);

		return m_pSample;
	}

	void Sound::SetSample(ALLEGRO_SAMPLE *pSample)
	{
		if (pSample)
		{
			m_pSample = pSample;
		}
	}
}