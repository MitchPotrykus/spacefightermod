#pragma once
#include "Resource.h"

namespace KatanaEngine
{
	class Sound :public Resource
	{
	public:
		Sound();
		virtual ~Sound();

		virtual bool Load(const std::string &path, ResourceManager *pManager);


		virtual ALLEGRO_SAMPLE *GetAllegroSample() const { return m_pSample; }

	protected:

		virtual void SetSample(ALLEGRO_SAMPLE *pSample);

	private:

		static bool s_alAddonInitialized;

		ALLEGRO_SAMPLE *m_pSample;
	};
}